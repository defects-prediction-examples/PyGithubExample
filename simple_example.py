import os

from dotenv import load_dotenv, find_dotenv
from github import Github
load_dotenv(find_dotenv())

TOKEN = os.environ.get('TOKEN')
g = Github(TOKEN)

repo = g.get_user('SudoBobo').get_repo('socket-chat')

# take particular issue with bag label
issue = repo.get_issue(2)
print('issue title is', issue.title)
print('issues labels are', *issue.labels)


# find commit, that closed issue
closing_commit_sha = None

events = issue.get_events()
for issue_event in events:
    if issue_event.event == 'closed':
        closing_commit_sha = issue_event.commit_id

# find previous commits in branch, that is finished with issue-closing commit
closing_commit = repo.get_commit(closing_commit_sha)
closing_commit_parents = closing_commit.parents

ls = list(closing_commit_parents)
print('total number of commits in brunch:', len(ls) + 1)


